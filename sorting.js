function positivePrioritySorting(a, b) {
  return a.endDateTime - b.endDateTime;
}

function nonPositivePrioritySorting(a, b) {
  const ratingCompare = (b.rating || 0.5) - (a.rating || 0.5);
  if(ratingCompare !== 0) return ratingCompare;
  else return a.name.localeCompare(b.name);
}

function rootSorting(a, b) {
  if (a.priority !== b.priority) return b.priority - a.priority;
  else if(a.priority > 0) {
    return positivePrioritySorting(a, b);
  } else {
    return nonPositivePrioritySorting(a, b);
  }
}

function sortGiveaways() {
  scanGiveaways();
  startUpdater();
  giveaways.sort(rootSorting);
  for (let i = 0; i < giveaways.length; i++) {
    giveaways[i].html.parentNode.appendChild(giveaways[i].html);
  }
}

loadedModules++;
