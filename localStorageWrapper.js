function unknownGameInfo() {
  return {
    priority   : 0,
    lastUpdated: 0
  }
}

function toSteamUrl(url) {
  return url.replace(/(^\w+:|^)\/\//, 'steam://')
}

function getGameInfo(url) {
  return JSON.parse(localStorage.getItem(toSteamUrl(url))) || unknownGameInfo()
}

function setGameInfo(url, newData) {
  localStorage.setItem(toSteamUrl(url), JSON.stringify(newData));
}

function fixStorage() {
  function findOldKeyIndex() {
    for (let i = 0; i <= localStorage.length; i++) {
      const key = localStorage.key(i)

      if (key && key.startsWith("steamhttp")) return i
    }

    return null
  }

  function fixKey(i) {
    const key = localStorage.key(i)
    const value = localStorage.getItem(key)

    localStorage.removeItem(key)

    if(value.priority !== 0) {
      const fixedKey = toSteamUrl(key)
      localStorage.setItem(fixedKey, value)
    }
  }

  let nextKey = findOldKeyIndex()
  while (typeof(nextKey) === "number") {
    fixKey(nextKey)
    nextKey = findOldKeyIndex()
  }
}

function downloadStorage() {
  let data = []
  for (let i = 0; i <= localStorage.length; i++) {
    const key = localStorage.key(i)
    if (!key || !key.startsWith("steam")) continue
    const value = localStorage.getItem(key)
    data.push(Object.assign({key}, JSON.parse(value)))
  }

  const file = new Blob([JSON.stringify(data, null, 2)]),
        a    = document.createElement("a"),
        url  = URL.createObjectURL(file)
  a.href     = url
  a.download = "steamgiftsData,json"
  document.body.appendChild(a)
  a.click()
  setTimeout(function() {
    document.body.removeChild(a)
    window.URL.revokeObjectURL(url)
  }, 0)

}

loadedModules++;
