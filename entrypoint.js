let giveaways = [];

function initSteamGiftsHelpers() {
  const topMenu    = $("div.nav__left-container");
  const menuItem   = $('<div class="nav__button-container">');
  const menuButton = $('<a class="nav__button" id="btnSettings">Sort</a>');
  topMenu.append(menuItem);
  menuItem.append(menuButton);
  menuButton.click(sortGiveaways);
}

initSteamGiftsHelpers();
