const appUrlPrefix   = 'store.steampowered.com/app/';
const millisInDay    = 1000 * 60 * 60 * 24;
let updaterIsRunning = false;

function extractGameId(steamUrl = "") {
  const prefixIndex = steamUrl.indexOf(appUrlPrefix)

  if(prefixIndex >= 0) {
    return steamUrl.slice(prefixIndex + appUrlPrefix.length, steamUrl.length - 1);
  }
  else return null
}

function updateGameInfo(url, callback) {
  console.info("Let's update game info for " + url);
  try {
    const id             = extractGameId(url);
    const gameInfo       = getGameInfo(url);
    gameInfo.lastUpdated = Date.now();

    // if we can't retrieve id from url then just update lastUpdate time.
    if (id === null) {
      setGameInfo(url, gameInfo);
      callback()
    } else {
      $.get("https://steam-https-proxy.herokuapp.com/appreviews/" + id + "?json=1&language=all")
        .done(data => {
          data.query_summary = data.query_summary || {};
          gameInfo.totalReviews    = data.query_summary.total_reviews || 0;
          gameInfo.positiveReviews = data.query_summary.total_positive || 0;
          gameInfo.rating          = gameInfo.positiveReviews / gameInfo.totalReviews;
          setGameInfo(url, gameInfo);
          giveaways
            .filter(g => g.steamUrl === url)
            .forEach(updateGiveaway);
          callback();
        });
    }
  } catch (ex) {
    console.error(`Can't update game info for url ${url}`);
    throw ex;
  }
}

function chooseGameToExtractInfo() {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  const candidateToUpdate = giveaways
    .map(g => g.steamUrl)
    .filter(onlyUnique)
    .map(url => Object.assign({ url: url, lastUpdated: 0 }, getGameInfo(url)))
    .filter(g => g.lastUpdated < (Date.now() - millisInDay))
    .sort((a, b) => a.lastUpdated - b.lastUpdated)[0];

  if (candidateToUpdate) {
    updateGameInfo(candidateToUpdate.url, () => setTimeout(chooseGameToExtractInfo, 2 * 1000));
  } else {
    updaterIsRunning = false;
  }
}

function startUpdater() {
  if (!updaterIsRunning) chooseGameToExtractInfo();
  updaterIsRunning = true;
}

loadedModules++;
