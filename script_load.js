const currentUrl  = window.location.href;
let loadedModules = 0;
const loadingMax  = 10000;
let loading       = 0;

if (currentUrl === "https://www.steamgifts.com/" || currentUrl.startsWith("https://www.steamgifts.com/giveaways")) {
  $('<script type="text/javascript" src="http://127.0.0.1:8887/localStorageWrapper.js"></script>').appendTo("head");
  $('<script type="text/javascript" src="http://127.0.0.1:8887/htmlExtenders.js"></script>').appendTo("head");
  $('<script type="text/javascript" src="http://127.0.0.1:8887/parsing.js"></script>').appendTo("head");
  $('<script type="text/javascript" src="http://127.0.0.1:8887/priorityHandlers.js"></script>').appendTo("head");
  $('<script type="text/javascript" src="http://127.0.0.1:8887/sorting.js"></script>').appendTo("head");
  $('<script type="text/javascript" src="http://127.0.0.1:8887/steamInfoExtractor.js"></script>').appendTo("head");
  while (loadedModules < 6 && loading < loadingMax) {
    loading++;
  }
  $('<script type="text/javascript" src="http://127.0.0.1:8887/entrypoint.js"></script>').appendTo("head");
  $('<link rel="stylesheet" href="http://127.0.0.1:8887/steamgifts.css">').appendTo("head");
}
