function increasePriority(steamUrl, oldPriority, newPriority) {
  return function () {
    let resultPriority;

    if(oldPriority > newPriority) resultPriority = oldPriority;
    else if (newPriority > 0 && oldPriority < 0)  resultPriority = 0;
    else resultPriority = newPriority;

    updateGamePriority(steamUrl, resultPriority)
  }
}

function decreasePriority(steamUrl, oldPriority, newPriority) {
  return function () {
    let resultPriority;

    if(oldPriority < newPriority) resultPriority = oldPriority;
    else if (newPriority < 0 && oldPriority > 0) resultPriority = 0;
    else resultPriority = newPriority;

    updateGamePriority(steamUrl, resultPriority)
  }
}

function updateGamePriority(steamUrl, newPriority) {
  let gameInfo = getGameInfo(steamUrl);
  gameInfo.priority = newPriority;
  setGameInfo(steamUrl, gameInfo);

  giveaways
    .filter(g => g.steamUrl === steamUrl)
    .forEach(updateGiveaway);
}

loadedModules++;
