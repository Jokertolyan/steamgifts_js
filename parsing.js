function getPrice($elem) {
  const text   = $elem.find('.giveaway__heading span:last').text();
  const length = text.length;
  return parseInt(text.substring(1, length - 2));
}

function getName($elem) {
  return $elem.find('.giveaway__heading__name').text()
}

function getSteamUrl($elem) {
  return $elem
    .find('a.giveaway__icon')
    .filter((a, b) => b.href.startsWith('http://store.steampowered.com') || b.href.startsWith('https://store.steampowered.com'))
    .attr('href')
}

// IMPORTANT! result in seconds, not in ms!
function getEndDateTime($elem) {
  return $elem
    .find('.fa-clock-o ~ span[data-timestamp]')
    .attr('data-timestamp')
}

function calculateGiveawayRating(gameInfo) {
  if(gameInfo.totalReviews < 200) return 0.4;
  else return gameInfo.rating;
}

function updateGiveaway(giveaway) {
  const $elem    = $(giveaway.html);
  const steamUrl = getSteamUrl($elem);
  const gameInfo = getGameInfo(steamUrl);

  let res = {
    html       : giveaway.html,
    price      : getPrice($elem),
    name       : getName($elem),
    steamUrl   : steamUrl,
    endDateTime: getEndDateTime($elem),
    priority   : gameInfo.priority,
    rating     : calculateGiveawayRating(gameInfo)
  };

  updateGiveawayHtml(res);
  return res;
}


function parseOneGiveaway(giveawayHtml) {
  const res = updateGiveaway({html: giveawayHtml});
  addJoinButtonEvent(res);
  return res;
}

function scanGiveaways() {
  const exists = $("#posts").find(".giveaway__row-outer-wrap").get();
  giveaways    = exists.map(parseOneGiveaway);
}

loadedModules++;
