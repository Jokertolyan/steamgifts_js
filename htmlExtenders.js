function addJoinButtonEvent(giveaway) {
  const $elem = $(giveaway.html);
  const joinButton = $elem.find('input[value=Join]');
  if(joinButton.attr('data-priorityEvent') !== 'true') {
    joinButton.click(increasePriority(giveaway.steamUrl, giveaway.priority, 1));
    joinButton.attr('data-priorityEvent', true);
  }
}

function updateGiveawayHtml(giveaway) {
  const $elem = $(giveaway.html);
  const iconsContainer  = $elem.find('.giveaway__heading');

  //cleanup
  const oldPriority = $elem.attr('data-priority') || 0;
  $elem.removeClass("priority" + oldPriority);
  iconsContainer.find('button.priority-button').remove();
  iconsContainer.find('span.rating').remove();

  //update
  $elem.attr('data-priority', giveaway.priority);
  $elem.addClass("priority" + giveaway.priority);
  const likeButton    = $('<button class="giveaway__icon priority-button"><i class="fa fa-thumbs-up"></i></button>');
  const dislikeButton = $('<button class="giveaway__icon priority-button"><i class="fa fa-thumbs-down"></i></button>');

  const ratingSpan = $(`<span class="giveaway__heading__thin rating">${Math.round(giveaway.rating * 100, -2)}%</span>`);
  likeButton.click(increasePriority(giveaway.steamUrl, giveaway.priority, 2));
  dislikeButton.click(decreasePriority(giveaway.steamUrl, giveaway.priority, -2));
  iconsContainer.append(likeButton).append(dislikeButton).append(ratingSpan);
}

loadedModules++;
